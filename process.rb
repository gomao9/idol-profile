# frozen_string_literal: true

require 'yaml'
require 'sparql/client'

IMAS    = RDF::URI('https://sparql.crssnky.xyz/imasrdf/URIs/imas-schema.ttl#')
FOAF    = RDF::URI('http://xmlns.com/foaf/0.1/')
SCHEMA  = RDF::URI('http://schema.org/')
RDFS    = RDF::URI('http://www.w3.org/2000/01/rdf-schema#')
DUMP_FILE = 'idols.yml'
DIR = 'idols'

def idols(client)
  return YAML.load_file(DUMP_FILE) if File.exist? DUMP_FILE

  sols = client.select.
    where([:idol, 'a', :class]).
    optional([:idol, FOAF + 'age',:age]).
    optional([:idol, IMAS + 'BloodType', :bloodType]).
    optional([:idol, SCHEMA + 'name', :name]) { |q| q.filter("lang(?name) = 'ja'") }.
    optional([:idol, SCHEMA + 'alternateName', :nickname]) { |q| q.filter("lang(?nickname) = 'ja'") }.
    optional([:idol, IMAS + 'alternateNameKana', :nicknameKana]).
    optional([:idol, SCHEMA + 'birthDate', :birthDate]).
    optional([:idol, SCHEMA + 'birthPlace', :birthPlace]).
    optional([:idol, SCHEMA + 'familyName', :familyName]) { |q| q.filter("lang(?familyName) = 'ja'") }.
    optional([:idol, IMAS + 'familyNameKana', :familyNameKana]).
    optional([:idol, SCHEMA + 'gender', :gender]).
    optional([:idol, SCHEMA + 'givenName', :givenName]) { |q| q.filter("lang(?givenName) = 'ja'") }.
    optional([:idol, IMAS + 'givenNameKana', :givenNameKana]).
    optional([:idol, SCHEMA + 'height', :height]).
    optional([:idol, IMAS + 'nameKana', :nameKana]).
    optional([:idol, SCHEMA + 'weight', :weight]).
    optional([:idol, IMAS + 'Attribute',:attribute]).
    optional([:idol, IMAS + 'BloodType',:bloodType]).
    optional([:idol, IMAS + 'Category',:category]).
    optional([:idol, IMAS + 'Color',:color]).
    optional([:idol, IMAS + 'Division',:division]).
    optional([:idol, IMAS + 'Handedness',:handedness]).
    optional([:idol, IMAS + 'ShoeSize',:shoeSize]).
    optional([:idol, IMAS + 'Type',:type]).
    optional([:idol, IMAS + 'Bust',:bust]).
    optional([:idol, IMAS + 'Waist',:waist]).
    optional([:idol, IMAS + 'Hip',:hip]).
    optional([:idol, IMAS + 'Constellation',:constellation]).
    filter("?class = <#{IMAS}Idol> || ?class = <#{IMAS}Staff>").
    filter("?idol != <https://sparql.crssnky.xyz/imasrdf/RDFs/detail/Producer>").
    limit(1000).solutions

  idols = sols.map do |s|
    hash = s.to_h.transform_values(&:value)
    [hash[:idol], hash]
  end.to_h
  File.open(DUMP_FILE, 'w') { |f| YAML.dump(idols, f) }

  idols
end

def query_idols(times = 1000)
  client = SPARQL::Client.new('https://sparql.crssnky.xyz/spql/imas')
  idols = idols(client)

  begin
    times.times do
      (k, v = idols.shift)
      break unless k

      idol = RDF.Resource(k)
      hobbies = client.select.where([idol, IMAS + 'Hobby', :hobby]).solutions.map { |s| s.hobby.value }
      talents = client.select.where([idol, IMAS + 'Talent', :talent]).solutions.map { |s| s.talent.value }
      favorites = client.select.where([idol, IMAS + 'Favorite', :favoirte]).solutions.map { |s| s.favoirte.value }
      v.merge!(hobby: hobbies.join(', '),
               talent: talents.join(', '),
               favorite: favorites.join(', '))
      file = "#{DIR}/#{URI.parse(k).path.split('/').last}.yml"
      File.open(file, 'w') { |f| YAML.dump(v, f) }
      puts "remain size: #{idols.length}"
      sleep 0.1
    end
  rescue => e
    pp e
    idols[k] = v
  ensure
    File.open(DUMP_FILE, 'w') { |f| YAML.dump(idols, f) }
  end

  puts "remain size: #{idols.length}"
end

def dump
  File.open('profile.yml', 'w') do |f|
    idols = Dir.children(DIR).inject({}) do |h, filename|
      i = YAML.load_file(File.join(DIR, filename))
      i.delete_point_zero!(:bust)
      i.delete_point_zero!(:waist)
      i.delete_point_zero!(:hip)
      i.delete_point_zero!(:height)
      i.delete_point_zero!(:weight)
      i[:birthDate] = i[:birthDate]&.delete_prefix('--')&.sub('-', '/')

      h.merge(i[:idol] => i)
    end
    YAML.dump(idols, f)
  end
end

class Hash
  def delete_point_zero!(key)
    self[key] &&= self[key].to_s.delete_suffix('.0')
  end
end

query_idols
dump
